# phoenix.rs

Rust based implementation of script to rebuild skyhawk infrastructure

**The rust compiler does NOT need to be installed to use this tool.**

## Usage

To pull the latest binary artifact from S3 for your platform and run it (requires AWS cli be installed):

```sh
aws s3 cp s3://udemy-stream-tools/phoenix-rising.sh - | sh               # Default: runs against sandbox
aws s3 cp s3://udemy-stream-tools/phoenix-rising.sh - | sh -s --help     # Show help text
aws s3 cp s3://udemy-stream-tools/phoenix-rising.sh - | sh -s --prod     # Run against prod account
aws s3 cp s3://udemy-stream-tools/phoenix-rising.sh - | sh -s -y         # Skip confirmation before terraform apply
aws s3 cp s3://udemy-stream-tools/phoenix-rising.sh - | sh -s --ssh-key=/some/other/path    # Use different SSH key
```

The `--prod`, `--ssh-key=<file>` and `-y` flags can all combined. Ex:

```
aws s3 cp s3://udemy-stream-tools/phoenix-rising.sh - | sh -s --prod --ssh-key=~/.ssh/special_rsa -y
```

That command would run against prod without prompting and try to do a git clone using ~/.ssh/special_rsa.

### Flags In-Depth

 * `--prod`: By default, this will run against sandbox. You must set this flag to apply to prod.
 * `--ssh-key=<file>`: By default, ~/.ssh/id_rsa is used as the private key for the git clone operation. Use this flag to specify a different private ssh key for the git clone. The public key must also used but the path is derived automatically by changing the filename extension to ".pub".
 * `-y`: Without this flag, you will be prompted after a plan is generated but before applying. Skip that confirmation by using this flag.

### Bash Alias

If you find yourself doing this often, consider adding this bash alias:

```sh
alias phoenix='aws s3 cp s3://udemy-stream-tools/phoenix-rising.sh - | sh -s'
```

Then, simply type:

```sh
phoenix          # Runs against sandbox
phoenix --prod   # Runs against prod
phoenix --help   # Prints usage instructions
```

## Requirements

To use this tool, you'll need the following binaries installed in your `$PATH`:

 * make
 * The 1password CLI `op`
 * jq
 * terraform
 * curl

Except for make, these tools are required by the skyhawk-infrastructure repo and are not automatically installed for you.

The following credentials are also required:

 * SSH key with read access to https://github.com/udemy/skyhawk-infrastructure
 * Valid AWS credentials which can assume the appropriate role for the Stream Team Prod and Sandbox accounts
 * 1Password credentials to retrieve RDS MySQL password and Sentry DSN (prompted interactively by skyhawk-infrastructure Makefile)

## Features

 * [x] Add flags to control which environment to run in (prod/sandbox)
 * [x] Default/configurable private key for git ssh
 * [x] create new directory
 * [x] clone singularity-infrastructure repo
 * [x] setup AWS STS credentials
 * [x] terraform plan
 * [x] terraform apply
 * [x] singularity configuration setup
 * [x] Manually push prebuilt OSX binary to Gitlab for now
 * [x] Add one-line bash script that downloads and executes this program built from CI (so other devs don't need to learn rust/install compiler)

## Developing Phoenix

There are abundant resources if you're trying to develop rust code and get stuck. The best ones are:

 * [Rust Beginners IRC channel](https://chat.mibbit.com/?server=irc.mozilla.org&channel=%23rust-beginners) -- a no shame place to get answers from real people in real time
 * [Rust SubReddit](https://www.reddit.com/r/rust) for slightly longer term questions
 * [Standard Library Documentation](https://doc.rust-lang.org/std/)
 * [The Rust Programming Language Book](https://doc.rust-lang.org/book/second-edition/index.html)

### Setup Development Environment

Install the rust toolchain using [rustup](https://rustup.rs/).

Rustup will install the main tool `cargo` which can run tests, compile the code, generate documentation and more.

Next install `rustfmt` to format whitespace/codestyle according to Rust community guidelines automatically:

```
rustup component add rustfmt-preview
```

### Compiling and Running

To compile and run phoenix use:

```sh
cargo run
```

If you want to pass flags to phoenix you can do so like this:

```
cargo run -- --prod
```


### Formatting Code

```
cargo fmt
```

## Publishing To S3

When you're happy with your code, use the helper script to create and upload an optimized build:

```
./publish-binary.sh
```

## TODO:

 * [ ] Check for/install depedencies (terraform, jq, curl, 1pass cli)
 * [ ] save plan to permanent directory
 * [ ] save logs to permanent directory
 * [ ] Add circleci for building rust binary on macos
