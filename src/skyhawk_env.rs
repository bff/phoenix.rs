use std::error::Error;
use std::fmt::{Display, Formatter, Result as FmtResult};

use rusoto_core::request::HttpClient;
use rusoto_core::Region;
use rusoto_credential::StaticProvider;
use rusoto_elb::{DescribeAccessPointsInput, Elb, ElbClient};
use rusoto_sts::{AssumeRoleRequest, AssumeRoleResponse, Credentials, Sts, StsClient};

/// This type exists so that we don't directly use rusoto types in our signature.
///
/// If their API changes, ours won't!
#[derive(Clone)]
pub struct AwsCredentials {
    pub access_key_id: String,
    pub secret_access_key: String,
    pub session_token: String,
}

impl From<Credentials> for AwsCredentials {
    fn from(response: Credentials) -> AwsCredentials {
        AwsCredentials {
            access_key_id: response.access_key_id,
            secret_access_key: response.secret_access_key,
            session_token: response.session_token,
        }
    }
}

#[derive(Clone, Copy)]
pub enum SkyhawkEnv {
    Prod,
    Sandbox,
}

impl Display for SkyhawkEnv {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(
            f,
            "{}",
            match *self {
                SkyhawkEnv::Prod => "prod",
                SkyhawkEnv::Sandbox => "sandbox",
            }
        )
    }
}

impl SkyhawkEnv {
    pub fn role(&self) -> String {
        match *self {
            SkyhawkEnv::Prod => "production-stream".into(),
            SkyhawkEnv::Sandbox => "streamsandbox".into(),
        }
    }

    pub fn account_id(&self) -> String {
        match *self {
            SkyhawkEnv::Prod => "053642684961".into(),
            SkyhawkEnv::Sandbox => "779209261487".into(),
        }
    }

    pub fn arn(&self) -> String {
        format!("arn:aws:iam::{}:role/{}", &self.account_id(), &self.role())
    }

    pub fn has_valid_ssl_cert(&self) -> bool {
        match *self {
            SkyhawkEnv::Prod => true,
            SkyhawkEnv::Sandbox => false,
        }
    }

    pub fn assume_aws_role(&self) -> Result<AwsCredentials, Box<Error>> {
        // TODO -- Add timeout: https://rusoto.github.io/rusoto/rusoto_core/struct.RusotoFuture.html#method.with_timeout
        let sts = StsClient::new(Region::UsEast1);
        let response =
            sts.assume_role(AssumeRoleRequest {
                role_arn: self.arn(),
                role_session_name: "udemy-session".into(),
                ..AssumeRoleRequest::default()
            }).sync()?;

        if let AssumeRoleResponse {
            credentials: Some(creds),
            ..
        } = response
        {
            Ok(creds.into())
        } else {
            Err("No credentials returned".into())
        }
    }

    pub fn hostname(&self, creds: AwsCredentials) -> Result<String, Box<Error>> {
        match *self {
            SkyhawkEnv::Prod => Ok("skyhawk.udsrv.com".into()),
            SkyhawkEnv::Sandbox => {
                // Setup an ELB API client with our temporary account specific credentials
                let credential_provider = StaticProvider::new(
                    creds.access_key_id,
                    creds.secret_access_key,
                    Some(creds.session_token),
                    None,
                );
                let request_dispatcher = HttpClient::new()?;
                let client =
                    ElbClient::new_with(request_dispatcher, credential_provider, Region::UsEast1);

                // Query the ELB API for the load balancer
                let response = client
                    .describe_load_balancers(DescribeAccessPointsInput {
                        load_balancer_names: Some(vec!["Skyhawk".into()]),
                        marker: None,
                        page_size: None,
                    })
                    .sync()?;

                // Lots of really annoying unwrapping of deeply nested data structures. Oh, AWS.....
                if response.load_balancer_descriptions.is_none() {
                    return Err("No load balancers found".into());
                }

                let load_balancers = response.load_balancer_descriptions.unwrap();
                if load_balancers.len() < 1 {
                    return Err("No load balancers found".into());
                }

                let load_balancer = &load_balancers[0];
                match load_balancer.canonical_hosted_zone_name {
                    Some(ref hostname) => Ok(hostname.clone()),
                    None => Err("No hostname found for ELB on sandbox :sob:".into()),
                }
            }
        }
    }
}
