use std::error::Error;
use std::thread::sleep;
use std::time::Duration;

use curl;

pub fn poll_until_singularity_up(
    hostname: &str,
    verify_certificate: bool,
) -> Result<(), Box<Error>> {
    let endpoint = format!("https://{}/singularity/api/state", hostname);
    let mut request = curl::easy::Easy::new();
    request.url(&endpoint)?;
    request.get(true)?;
    request.ssl_verify_host(verify_certificate)?;
    request.ssl_verify_peer(verify_certificate)?;

    let mut delay_seconds = Duration::new(1, 0);
    for _ in 0..15 {
        if request.perform().is_err() {
            eprintln!("Unable to connect to {}", &endpoint);
            continue;
        }

        match request.response_code() {
            Ok(status_code) => {
                // Stop retrying once we get a successful response
                if status_code == 200 {
                    eprintln!("Successfully connected to {}", &endpoint);
                    return Ok(());
                }

                eprintln!("Non-200 status code {}", status_code);
            }
            Err(err) => eprintln!("{}", err),
        }

        // Doubles backoff
        sleep(delay_seconds);
        delay_seconds *= 2;
    }
    Err("Exhausted max retries waiting for Singularity to come online".into())
}
