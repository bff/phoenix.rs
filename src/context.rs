use std::error::Error;
use std::io::stdin;
use std::path::Path;
use std::process::Command;

use mktemp::Temp;

use super::git::clone_repo;
use super::singularity::poll_until_singularity_up;
use super::skyhawk_env::{AwsCredentials, SkyhawkEnv};

/// Handles all the icky details of getting AWS credentials, a temp directory, and cleanup while
/// exposing makefile targets (Ex: terraform_plan) without repetitive typing of environment and
/// path, etc.
pub struct Context {
    temp: Temp,
    credentials: AwsCredentials,
    skyhawk_env: SkyhawkEnv,
    keep_temp_on_failure: bool,
}

/// At context destruction time, decide whether or not to destroy the temp directory.
/// This lets us avoid writing tons and tons of boilerplate on every method of Context.
/// See the std library [documentation on Drop](https://doc.rust-lang.org/std/ops/trait.Drop.html)
/// for more details on this trait.
impl Drop for Context {
    fn drop(&mut self) {
        // By default, the mktemp library will delete the temp directory inside its own
        // implementation of Drop.
        if self.keep_temp_on_failure {
            self.temp.release();
            eprintln!(
                "Temp directory is not deleted at: {:?}",
                &self.temp.to_path_buf()
            );
        }
    }
}

impl Context {
    /// Given an environment, attempt to obtain AWS credentials, a temp directory to work in, and a
    /// cloned copy of skyhawk-infrastructure.
    pub fn new(skyhawk_env: SkyhawkEnv) -> Result<Context, Box<Error>> {
        let mut temp = Temp::new_dir()?;
        eprintln!("Created temp dir at {:?}", &temp.to_path_buf());

        let credentials = skyhawk_env.assume_aws_role()?;
        eprintln!("Obtained AWS credentials for {}", skyhawk_env);

        Ok(Context {
            credentials: credentials,
            skyhawk_env: skyhawk_env,
            temp: temp,
            keep_temp_on_failure: false,
        })
    }

    /// Mutates the context's cleanup behavior.
    pub fn keep_temp_on_failure(&mut self, keep_it: bool) {
        self.keep_temp_on_failure = keep_it;
    }

    /// Using the temp directory for this context, do git clone into that path using the specified
    /// SSH key.
    pub fn clone_repo<P: AsRef<Path>>(&self, ssh_key: P) -> Result<(), Box<Error>> {
        clone_repo(self.temp.to_path_buf(), ssh_key.as_ref().to_path_buf())
    }

    /// Run a specific make target in this "context". On failure, leave the temp directory behind.
    pub fn make(&self, target: &str) -> Result<(), Box<Error>> {
        eprintln!("Running `make {} ENV={}`", target, self.skyhawk_env);
        Command::new("make")
            .env("AWS_ACCESS_KEY_ID", &self.credentials.access_key_id)
            .env("AWS_SECRET_ACCESS_KEY", &self.credentials.secret_access_key)
            .env("AWS_SESSION_TOKEN", &self.credentials.session_token)
            .current_dir(self.temp.to_path_buf())
            .arg(target)
            .arg(format!("ENV={}", self.skyhawk_env))
            .status()?;
        Ok(())
    }

    pub fn poll_until_singularity_up(&self) -> Result<(), Box<Error>> {
        let hostname = self.skyhawk_env.hostname(self.credentials.clone())?;
        let verify_ssl_certs = self.skyhawk_env.has_valid_ssl_cert();
        poll_until_singularity_up(&hostname, verify_ssl_certs)
    }

    pub fn confirm(&self, prompt: &str) -> Result<(), Box<Error>> {
        let mut input = String::new();
        loop {
            eprintln!("{}", prompt);
            stdin().read_line(&mut input)?;
            if input.contains("y") {
                return Ok(());
            } else if input.contains("n") {
                return Err("Aborted".into());
            }
            eprintln!("Invalid choice. Enter y or n");
        }
    }
}
