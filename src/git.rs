use std::error::Error;
use std::path::{Path, PathBuf};

use git2::build::RepoBuilder;
use git2::{Cred, CredentialType, Error as GitError, FetchOptions, RemoteCallbacks};

static GIT_URL: &'static str = "git@github.com:udemy/skyhawk-infrastructure.git";

pub fn clone_repo<P: AsRef<Path>>(local_path: P, private_key: P) -> Result<(), Box<Error>> {
    // Setup SSH keys for the git clone from github.
    let mut remote_callbacks = RemoteCallbacks::new();
    remote_callbacks.credentials(
        |_: &str, user_from_url: Option<&str>, _: CredentialType| -> Result<Cred, GitError> {
            // If a username cannot be found in the GIT_URL, default to using "git"
            let user = user_from_url.unwrap_or("git");

            // Take the private key passed into this function and make the public key have the extension
            // set to .pub. Ex: id_rsa private key -> public key id_rsa.pub
            let mut public_key = PathBuf::from(private_key.as_ref());
            public_key.set_extension("pub");
            eprintln!("Pubic key at {:?}", &public_key);

            // Despite the Option type for the public key, public_key IS required by libgit2.
            Cred::ssh_key(user, Some(public_key.as_ref()), private_key.as_ref(), None)
        },
    );
    let mut fetch_options = FetchOptions::new();
    fetch_options.remote_callbacks(remote_callbacks);

    // Actually clone the repo.
    eprintln!("Starting clone of {}", GIT_URL);
    RepoBuilder::new()
        .fetch_options(fetch_options)
        .clone(GIT_URL, local_path.as_ref())?;
    eprintln!("Finished clone of {}", GIT_URL);

    Ok(())
}
