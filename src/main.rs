#![allow(unused_mut)]

extern crate curl;
extern crate docopt;
extern crate git2;
extern crate mktemp;
extern crate rusoto_core;
extern crate rusoto_credential;
extern crate rusoto_elb;
extern crate rusoto_sts;
extern crate shellexpand;

mod context;
mod git;
mod singularity;
mod skyhawk_env;

use std::error::Error;

use docopt::Docopt;

use context::Context;
use skyhawk_env::SkyhawkEnv;

const USAGE: &'static str = "
Udemy Phoenix. A statically compiled binary for (re)building Skyhawk from its terraform config.

Usage:
  phoenix [--prod] [--ssh-key=<path>] [-y]
  phoenix (-h | --help)

Options:
  -h --help         Show this screen
  -y                Skip confirmation to run `make terraform_apply`
  --prod            Rebuild Skyhawk in the Prod AWS account (defaults to sandbox)
  --ssh-key=<path>  Path to private key for SSH (defaults to ~/.ssh/id_rsa)
";

fn main() -> Result<(), Box<Error>> {
    // Parse flags passed on the command line
    let args = Docopt::new(USAGE)
        .and_then(|dopt| dopt.parse())
        .unwrap_or_else(|e| e.exit());

    // Decide which environment to execute terraform commands against based on input.
    // Default to sandbox.
    let skyhawk_env = match args.get_bool("--prod") {
        true => SkyhawkEnv::Prod,
        false => SkyhawkEnv::Sandbox,
    };
    eprintln!("Running phoenix against {}", skyhawk_env);

    // Decide which SSH private key to use based on flags. Default to ~/.ssh/id_rsa
    // Public key path is created by appending .pub to private key path deeper in application.
    let private_key = shellexpand::full(match args.get_str("--ssh-key") {
        "" => "~/.ssh/id_rsa",
        _ => args.get_str("--ssh-key"),
    })?;
    eprintln!("Using private ssh key {:?}", &private_key);

    // Run the application
    let mut context = Context::new(skyhawk_env)?;
    context.clone_repo(private_key.as_ref())?;
    context.make("terraform_plan")?;
    if !args.get_bool("-y") {
        context.confirm("Are you sure want to apply this plan?")?;
    }
    context.keep_temp_on_failure(true);
    context.make("terraform_apply")?;
    context.poll_until_singularity_up()?;
    context.make("singularity_config")?;

    // Say Adios!
    eprintln!(
        "Finished raising {}, like a phoenix from the ashes",
        skyhawk_env
    );
    Ok(())
}
