#!/bin/bash -eux
set -o pipefail

cargo build --release
aws s3 cp target/release/udemy-phoenix "s3://udemy-stream-tools/$(uname -s)/phoenix"
aws s3 cp shim.sh "s3://udemy-stream-tools/phoenix-rising.sh"
