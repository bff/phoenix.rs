#!/bin/bash -e
set -o pipefail

aws s3 cp "s3://udemy-stream-tools/$(uname -s)/phoenix" /usr/local/bin/phoenix
chmod +x /usr/local/bin/phoenix
phoenix "${@}"
